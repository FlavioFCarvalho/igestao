package com.igestao.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.igestao.model.Lancamento;

public class Lancamentos implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private EntityManager manager;

	@Inject
	public Lancamentos(EntityManager manager) {
	this.manager = manager;
	}

	public void adicionar(Lancamento lancamento) {
		this.manager.persist(lancamento);
		}
	
	public List<Lancamento> todos() {
		TypedQuery<Lancamento> query = manager.createQuery("from Lancamento",
				Lancamento.class);
		return query.getResultList();
	}

}
